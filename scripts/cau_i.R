# các packages cần phải cài đặt
#install.packages("tidyverse")
#install.packages("anytime")
#install.packages("lubridate")
#install.packages("dplyr")

library(anytime)
library(lubridate)
library(dplyr)

# phải load general_env để chạy đúng
# data <- read.csv("owid-covid-data.csv") # đã lưu vào enviroments

#câu i/1
date <- data[,4]
date <- anytime::anydate(date)
vector <- lubridate::year(date)
unique(vector)

#câu i/2
result <- unique(data[,c(1,3)])
head(result,10)
count_country <- count(result)

#câu i/3: Số lượng châu lục trong tập mẫu
continents <- data[c("continent")]
table_continents <- table(continents)
result3 <- count(table_continents)
result3

#câu i/4: Số lượng dữ liệu thể hiện thu thập dữ liệu được trong từng từng châu lục và tổng số
continents_and_case <- unique(data[,c("continent","new_cases")])
groupContinent <- continents_and_case %>% group_by(continent)
sumData <- aggregate(.~continent,groupContinent,sum)
sumTotal <- sum(sumData[, 'new_cases'])
sumData
sumTotal

#câu i/5: Số lượng dữ liệu thể hiện thu thập dữ liệu được trong từng đất nước (hiển thị 10 dất nước cuối cùng) và tổng số
iso_and_case <- unique(data[,c("iso_code","new_cases")])
groupIso <- iso_and_case %>% group_by(iso_code)
sumData <- aggregate(.~iso_code,groupIso,sum)
sumTotal <- sum(sumData[, 'new_cases'])
result5<-tail(sumData)
result5
sumTotal

#câu 6: Cho biết các châu lục nào có lượng dữ liệu thu thập nhỏ nhất và giá trị nhó nhất đó?
columnForAppend <- unique(data[,c("continent","new_cases")])
groupContinent <- columnForAppend %>% group_by(continent)
sumData <- aggregate(.~continent,groupContinent,sum)
resultOrder<-sumData[
  with(sumData, order(new_cases)),
]
result6<-head(resultOrder)
result6

#câu 7: Cho biết các châu lục nào có lượng dữ liệu thu thập lớn nhất và giá trị lớn nhất đó?
columnForAppend <- unique(data[,c("continent","new_cases")])
groupContinent <- columnForAppend %>% group_by(continent)
sumData <- aggregate(.~continent,groupContinent,sum)
resultOrder<-sumData[
  with(sumData, order(new_cases,decreasing=TRUE)),
]
result7<-head(resultOrder)
result7

#câu 8: Cho biết các nước nào có lượng dữ liệu thu thập nhỏ nhất và giá trị nhó nhất đó?
columnForAppend <- unique(data[,c("location","new_cases")])
groupLocation <- columnForAppend %>% group_by(location)
sumData <- aggregate(.~location,groupLocation,sum)
resultOrder<-sumData[
  with(sumData, order(new_cases)),
]
result8<-head(resultOrder)
result8

#câu 9: Cho biết các nước nào có lượng dữ liệu thu thập lớn nhất và giá trị lớn nhất đó?
columnForAppend <- unique(data[,c("location","new_cases")])
groupLocation <- columnForAppend %>% group_by(location)
sumData <- aggregate(.~location,groupLocation,sum)
resultOrder<-sumData[
  with(sumData, order(new_cases,decreasing=TRUE)),
]
result9<-head(resultOrder)
result9

#câu 10: Cho biết các date nào có lượng dữ liệu thu thập nhỏ nhất và giá trị nhó nhất đó?
columnForAppend <- unique(data[,c("date","new_cases")])
groupLocation <- columnForAppend %>% group_by(date)
sumData <- aggregate(.~date,groupLocation,sum)
resultOrder<-sumData[
  with(sumData, order(new_cases)),
]
result10<-head(resultOrder)
result10

#câu 11: Cho biết các date nào có lượng dữ liệu thu thập lớn nhất và giá trị lớn nhất đó?
columnForAppend <- unique(data[,c("date","new_cases")])
groupLocation <- columnForAppend %>% group_by(date)
sumData <- aggregate(.~date,groupLocation,sum)
resultOrder<-sumData[
  with(sumData, order(new_cases,decreasing=TRUE)),
]
result11<-head(resultOrder)
result11

#câu 12: Cho biết số lượng dữ liệu thu thập được theo date và châu lục.
result12 <- unique(data[,c("continent","date","new_cases")])
result12

#câu 13: Cho biết số lượng dữ liệu thu thập được là lớn nhất theo date và châu lục
columnForResult <- unique(data[,c("continent","date","new_cases")])
maxForDate <- max(columnForResult$new_cases)
"Max new cases of date and contitent"
maxForDate
"Contitent"
columnForResult[which.max(columnForResult$new_cases),1]
"Date"
columnForResult[which.max(columnForResult$new_cases),2]

#câu 14: Cho biết số lượng dữ liệu thu thập được là nhỏ nhất theo date và châu lục
columnForResult <- unique(data[,c("continent","date","new_cases")])
minForDate <- min(columnForResult$new_cases)
"Min new cases of date and contitent"
minForDate
"Contitent"
columnForResult[which.min(columnForResult$new_cases),1]
"Date"
columnForResult[which.min(columnForResult$new_cases),2]

#câu 15: Với một date là k và châu lục t cho trước, hãy cho biết số lượng dữ liệu thể hiện thu thập dữ liệu được
#cho date là k=2020-02-24
#cho châu lục t=Asia
#result: số lượng báo cáo
count_length_newcases = function (k,t) {
  continents_and_case <- unique(data[,c("continent","date","new_cases")])
  groupContinent <- filter(continents_and_case, date == k & continent == t)
  countContinent <- table(groupContinent$continent)
  return (countContinent)
}
count_length_newcases("2020-02-24","Asia")

#câu 16: Có đất nước nào mà số lượng dữ liệu thu thập được là bằng nhau không? Hãy cho biết các iso_code của đất nước đó
column_for_newCase <- unique(data[,c("iso_code","location","new_cases")])
sum_newCases <- aggregate(.~new_cases,column_for_newCase,sum)
sum_case<-sum_newCases[
  with(sum_newCases, order(x)),
]
result16<-sum_newCases[
  ,with(sum_newCases, order(new_cases)),
]
result16

#câu 17: Liệt kê iso_code, tên đất nước mà chiều dài iso_code lớn hơn 3
column_for_iso <- unique(data[,c("iso_code","location")])
result17 <- filter(column_for_iso, nchar(iso_code) >3)
result17


