#phải load general_env1 để chạy đúng
library(tidyverse)
library(dplyr)
library(ggplot2)

mygroupdata$Month_Year <- substr(mygroupdata$date, 1,7)
mygroupdata$day <- substr(mygroupdata$date, 9,10)

months_sq = c("2020-06","2020-07","2020-10","2021-06","2021-07","2021-10","2021-02","2022-02")
two_last_months_sq = c("2020-11","2020-12","2021-11","2021-12")
countrys_sq = c("Canada","Greenland","United States")

my_loop = function(func,folder) {
  
  for (country in countrys_sq)
  {
    for (month in months_sq)
    {
      # Open a pdf file
      pdf(paste(folder,country,"-",month,".pdf",sep = ""))
      # 2. Create a plot
      print(func(month,country))
      # Close the pdf file
      dev.off() 
    }
  }
}

my_loop_2_last_months = function(func,folder) {
  
  for (country in countrys_sq)
  {
    for (month in two_last_months_sq)
    {
      # Open a pdf file
      pdf(paste(folder,country,"-",month,".pdf",sep = ""))
      # 2. Create a plot
      print(func(month,country))
      # Close the pdf file
      dev.off() 
    }
  }
}

#cau v_1

v_1 = function (month, country) {
  graph <- mygroupdata %>%
    filter(location == country, Month_Year == month) %>%
    ggplot(aes(x=day,y=new_cases, group = 1)) +
    geom_point() +
    geom_line() +
    xlab(paste("New cases in", month, "in", country, sep = " ")) +
    ggtitle("Line Chart")
  return(graph)
}

my_loop(v_1,"./graph/v_1/")

# cau v_2

v_2 = function (month, country) {
  graph <- mygroupdata %>%
    filter(location == country, Month_Year == month) %>%
    ggplot(aes(x=day,y=new_deaths, group = 1)) +
    geom_point() +
    geom_line() +
    xlab(paste("New deaths in", month, "in", country, sep = " ")) +
    ggtitle("Line Chart")
  return(graph)
}

my_loop(v_2,"./graph/v_2/")

# cau v_3

v_3 = function (month, country) {
  dt <- mygroupdata %>% gather(keys, values, new_cases:new_deaths)
  
  graph <- dt %>%
    filter(location == country, Month_Year == month) %>%
    ggplot(aes(x = day, y = values)) +
    geom_col(aes(fill = keys)) +
    xlab(paste("New cases and New deaths in", month, "in", country, sep = " ")) +
    ggtitle("Column Chart")
  return(graph)
}

my_loop(v_3,"./graph/v_3/")

#cau v_4

my_loop_2_last_months(v_1,"./graph/v_4/")

#cau v_5

my_loop_2_last_months(v_2,"./graph/v_5/")

#cau v_6

my_loop_2_last_months(v_3,"./graph/v_6/")

#cau v_7

v_7 = function (month, country) {
  dt <- mygroupdata %>%
        filter(location == country, Month_Year == month)
  oldvalue = 0;
  for (row in 1:nrow(dt)) {
    dt[row,"tl"] <- dt[row,"new_cases"] +oldvalue
    oldvalue = dt[row,"tl"]
  }
  graph <- dt  %>%
    ggplot(aes(x=day,y=tl, group = 1)) +
    geom_point() +
    geom_line() +
    xlab(paste("Du lieu nhiem benh tich luy trong", month, "o", country, sep = " ")) +
    ggtitle("Line Chart")
  return(graph)
}

my_loop(v_7,"./graph/v_7/")

#cau v_8

v_8 = function (month, country) {
  dt <- mygroupdata %>%
    filter(location == country, Month_Year == month)
  oldvalue = 0;
  for (row in 1:nrow(dt)) {
    dt[row,"tl"] <- dt[row,"new_deaths"] +oldvalue
    oldvalue = dt[row,"tl"]
  }
  graph <- dt  %>%
    ggplot(aes(x=day,y=tl, group = 1)) +
    geom_point() +
    geom_line() +
    xlab(paste("Du lieu tu vong tich luy trong", month, "o", country, sep = " ")) +
    ggtitle("Line Chart")
  return(graph)
}

my_loop(v_8,"./graph/v_8/")

#cau vi

seven_day = function (n,type) {
  sum <- 0
  y <- 1
  for (x in 0:6) {
    if(n-x>=1) {
    if(mygroupdata[n,"Month_Year"] == mygroupdata[n-x,"Month_Year"]){
    sum <- sum + mygroupdata[n-x,type]
    y <- y + 1
    }
    }
  }
  return (round(sum/y, digits = 2))
}

for (row in 1:nrow(mygroupdata)) {
  mygroupdata[row,"svd_cases"] <- seven_day(row,"new_cases")
  mygroupdata[row,"svd_deaths"] <- seven_day(row,"new_deaths")
}

#cau vi_1

vi_1 = function (month, country) {
  graph <- mygroupdata %>%
    filter(location == country, Month_Year == month) %>%
    ggplot(aes(x=day,y=svd_cases, group = 1)) +
    geom_point() +
    geom_line() +
    xlab(paste("Mean of new cases in closest 7 days", month, "in", country, sep = " ")) +
    ggtitle("Line Chart")
  return(graph)
}

my_loop(vi_1,"./graph/vi_1/")

#cau vi_2

vi_2 = function (month, country) {
  graph <- mygroupdata %>%
    filter(location == country, Month_Year == month) %>%
    ggplot(aes(x=day,y=svd_deaths, group = 1)) +
    geom_point() +
    geom_line() +
    xlab(paste("Mean of new deaths in closest 7 days", month, "in", country, sep = " ")) +
    ggtitle("Line Chart")
  return(graph)
}

my_loop(vi_2,"./graph/vi_2/")

#cau vi_3

vi_3 = function (month, country) {
  dt <- mygroupdata %>% gather(keys, values, svd_cases:svd_deaths)
  
  graph <- dt %>%
    filter(location == country, Month_Year == month) %>%
    ggplot(aes(x = day, y = values)) +
    geom_col(aes(fill = keys)) +
    xlab(paste("Mean of new cases & new deaths in cloest 7 days", month, "in", country, sep = " ")) +
    ggtitle("Column Chart")
  return(graph)
}

my_loop(vi_3,"./graph/vi_3/")

#cau vi_4

my_loop_2_last_months(vi_1,"./graph/vi_4/")

#cau vi_5

my_loop_2_last_months(vi_2,"./graph/vi_5/")

#cau vi_6

my_loop_2_last_months(vi_3,"./graph/vi_6/")

#cau vi_7

vi_7 = function (month, country) {
  dt <- mygroupdata %>%
    filter(location == country, Month_Year == month)
  oldvalue = 0;
  for (row in 1:nrow(dt)) {
    dt[row,"tl"] <- dt[row,"svd_cases"] +oldvalue
    oldvalue = dt[row,"tl"]
  }
  graph <- dt  %>%
    ggplot(aes(x=day,y=tl, group = 1)) +
    geom_point() +
    geom_line() +
    xlab(paste("Du lieu nhiem benh tich luy trong", month, "o", country, sep = " ")) +
    ggtitle("Line Chart")
  return(graph)
}

my_loop(vi_7,"./graph/vi_7/")

#cau vi_8

vi_8 = function (month, country) {
  dt <- mygroupdata %>%
    filter(location == country, Month_Year == month)
  oldvalue = 0;
  for (row in 1:nrow(dt)) {
    dt[row,"tl"] <- dt[row,"svd_deaths"] +oldvalue
    oldvalue = dt[row,"tl"]
  }
  graph <- dt  %>%
    ggplot(aes(x=day,y=tl, group = 1)) +
    geom_point() +
    geom_line() +
    xlab(paste("Du lieu tu vong tich luy trong", month, "o", country, sep = " ")) +
    ggtitle("Line Chart")
  return(graph)
}

my_loop(vi_8,"./graph/vi_8/")
